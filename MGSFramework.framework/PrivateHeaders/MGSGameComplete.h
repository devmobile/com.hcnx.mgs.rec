//
//  MGSGameComplete.h
//  MGSFramework
//
//  Created by Guillaume MARTINEZ on 26/01/2018.
//  Copyright © 2018 HighConnexion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MGSGame.h"
#import "SmsContent.h"

@interface MGSGameComplete : MGSGame

- (id)initWithDictionary:(NSDictionary*)pDic;

/*@property (nonatomic, readonly) NSInteger mgs_id;
@property (nonatomic, retain, readonly) NSString * banner_url;
@property (nonatomic, retain,readonly) NSString * picto_asterisk_url;
@property (nonatomic, retain,readonly) NSString * picto_game_type_url;
@property (nonatomic, retain,readonly) NSString * picto_payment_url;
@property (nonatomic, retain,readonly) NSString * url;
@property (nonatomic, retain,readonly) NSString * title;
@property (nonatomic, retain,readonly) NSString * subtitle;
@property (nonatomic, retain,readonly) NSString * cgu;
@property (nonatomic, retain,readonly) NSDate * start_date;
@property (nonatomic, retain,readonly) NSDate * end_date;
@property (nonatomic, retain,readonly) NSString * game_format;
@property (nonatomic, retain,readonly) NSString * game_type;
@property (nonatomic, retain,readonly) NSString * phone_number;
@property (nonatomic, readonly) NSInteger payment_method;
@property (nonatomic, retain,readonly) NSString * pdf_game_rules;
@property (nonatomic, readonly) CGFloat price;
@property (nonatomic, retain,readonly) NSDictionary * raw_data;
@property (nonatomic, readonly) NSInteger highwin_id;
@property (nonatomic, readonly) NSInteger position;
@property (nonatomic, readonly) NSInteger shortcode;
@property (nonatomic, retain,readonly) SmsContent * sms_content;*/

@end
