//
//  MGSFramework.h
//  MGSFramework
//
//  Created by Damien PRACA on 03/10/16.
//  Copyright © 2016 HighConnexion. All rights reserved.
//

#ifndef MGSFramework_h
#define MGSFramework_h


#endif /* MGSFramework_h */

#import "MGSGame.h"
#import "MGSView.h"
#import "MGSViewController.h"
#import "SmsContent.h"

#import <UIKit/UIKit.h>
#import <HCNXBase/HCNXBase.h>

//! Project version number for MGSFramework.
FOUNDATION_EXPORT double MGSFrameworkVersionNumber;

//! Project version string for MGSFramework.
FOUNDATION_EXPORT const unsigned char MGSFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MGSFramework/PublicHeader.h>
#import <Foundation/Foundation.h>

@interface MGSFramework : HCNXBase<MFMessageComposeViewControllerDelegate, SFSafariViewControllerDelegate>

/*!
 @brief Use this method to get the instance of MGS
 */
+ (MGSFramework *)sharedInstance;

- (void)getGames:(void (^)(NSArray * games, NSError * error))onCompletion  __attribute__((deprecated("this method will be deleted in a next version.")));

- (void)getGamesV2:(void (^)(NSArray * games, NSError * error))onCompletion;

- (void)enableTestEnvironment;

- (void)play:(NSNumber *)pGame andAnswer:(NSString*)pAnswer  __attribute__((deprecated("this method will be deleted in a next version.")));

- (void)playV2:(MGSGame *)pGames andAnswer:(NSString*)pAnswer;

- (void)openRules:(NSNumber *)pIdGame __attribute__((deprecated("this method will be deleted in a next version.")));

- (void)showRules:(MGSGame *)pGame;

- (void)showLog:(BOOL)pActivated;

@end

