//
//  MGSViewController.h
//  testMGSViews
//
//  Created by Guillaume MARTINEZ on 24/01/2018.
//  Copyright © 2018 Guillaume MARTINEZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MGSViewControllerDelegate;

@interface MGSViewController : UINavigationController

@property (nonatomic, assign) id <MGSViewControllerDelegate> mDelegate;

- (instancetype)initWithType:(NSInteger)pType;
- (void)setNavBarTitle:(NSString*)pTitle;
- (void)setNavBarTintColor:(UIColor*)pColor;
- (void)setNavBarColor:(UIColor*)pColor;
- (void)setImageAsTitle:(UIImage*)pImage;
- (UIColor*)getNavBarColor;
- (BOOL)isDelegate;

@end



@protocol MGSViewControllerDelegate
@optional
- (void)dismiss:(MGSViewController*)controller;

@end
