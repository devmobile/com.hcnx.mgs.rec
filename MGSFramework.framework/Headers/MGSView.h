//
//  MGSView.h
//  MGSFramework
//
//  Created by Guillaume MARTINEZ on 18/01/2018.
//  Copyright © 2018 HighConnexion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCNXBase/HCNXBase.h>

@protocol MGSViewDelegate;

@interface MGSView : UIView

- (void)configureWithGame:(id)pMGSGame andType:(NSInteger)pType;
- (void)setTitleColor:(UIColor*)pColor;
- (void)prepareForReuse;
- (void)setBackgroundMGSViewColor:(UIColor*)pColor;
- (CGFloat)getHeight;

@property (nonatomic, assign) id <MGSViewDelegate> delegate;

@end

@protocol MGSViewDelegate

- (void)newHeight:(CGFloat)pHeight;

@end
