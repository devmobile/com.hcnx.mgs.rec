//
//  SmsContent.h
//  MGSFramework
//
//  Created by Guillaume MARTINEZ on 16/11/2016.
//  Copyright © 2016 HighConnexion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmsContent : NSObject

- (id)initWithDictionary:(NSDictionary*)pDic;

@property (nonatomic, retain, readonly) NSString * message;
@property (nonatomic, retain, readonly) NSArray * answers;

@end
